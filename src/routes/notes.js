//aqui iran las url de la pagina principal
const express = require('express');  //facilita creacion d ruta
const router = express.Router();

//PARA REQUERIR UNA NUEVA NOTA
const Note = require('../models/Notes');  //aqui ya puedes usar sus metodo para guardar generar nuevo etc




router.get('/notes/add',(req, res) =>{
    res.render('notes/new-note')
})

router.post('/notes/new-note', async (req, res) =>{  //este está igual al action q devuelve el archivo new-notes y lo redirecciona... se pone async para decirle q es asincrono, no va esperar la respuesta de la bd
    console.log(req.body);  //para pintar en consola

    const {title , description} = req.body;  //aquí sacas u obtienes los avlores de cada campo enviado del formulario
    const errors =[];
    if(!title){
        errors.push({text: 'PORFA, INSERTA UN TITULO'})
    }
    if(!description){
        errors.push({text: 'PORFA, INSERTA UNA DESCRIPTION'})
    }
    if(errors.length > 0){
        res.render('notes/new-note',{
            errors,
            title,
            description
        });
    }else{
        
        //aqui instancias esa constante la clase, al crear un escheca crear una clase, y aqui instancias para crear nuevo dato
       const newNote =  new Note({title,description});
      await  newNote.save();  //await: significa q le vas decir que no va esperar el exito de la BD PAR Q PROSIGA CON LO QUE ESTE DEBAJO.... para guardar en la BD sin eso aún no lo guarda... debes tener en cuenta en cuanto tiempo se llega A DEMORAR EN GUARDAR EN LA TABALA DE BD
       // YA QUE MONGO ES ASINCRONO, LE A TOMAR UNOS SEGUNDO, y para no esperar a que responda hay q decirle que es una peticion asincrona
       console.log(newNote);
       res.redirect('/notes'); //BB1 redirecciona a la otra pagina y es lo que va pintar en dicha pagina
    }

})


router.get('/notes', async (req, res) =>{ //BB1 aqui redireccionA NOTES Y TE MUESTRA LO ENVIADO EN SEND
   const notes =  await Note.find().lean().sort({date: 'desc'});  //((quiero buscar todo los datos dentro de la BD )) tambien puedes filtrarlo aqui.. recordar q esto tambien debe ser asincrono
    //ALEERTAAAAAA PARECE QUE HAY PALABRAS RESERVADAS COMO title, tuve que poner lean para que los obviara  https://stackoverflow.com/questions/59690923/handlebars-access-has-been-denied-to-resolve-the-property-from-because-it-is
   res.render('notes/all-notes', { notes }); //aquí le dices ue renderise la pagina all-notes y a la vez le pasas notes que es lo consultado en mongo

})


module.exports = router;