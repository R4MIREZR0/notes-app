//aqui iran las url de la pagina principal
const express = require('express');  //facilita creacion d ruta
const router = express.Router();

router.get('/', (req, res) => {
    res.render('index');
});

router.get('/about', (req, res) => {
    res.render('about');   //en vez de enviar (res.send('About')) ahora se pone render (renderiza o envia) por send para enviarle todo el archibo about.hbs
});

module.exports = router;