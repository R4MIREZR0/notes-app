//importar modulos
const express = require('express');
const path = require('path')
const exphbs = require('express-handlebars')
const methotOverride= require('method-override');
const session = require('express-session')
//INITIALIZACIONS
const app = express();
require('./database');


//SERVIDOR SE DIVIDIRÁ EN VARIAS SECCIONES;:************************

//SETTING   toda la configuraciones

app.set('port',process.env.PORT || 3000)  //puerto q escuchara. A veces la nube te ofrece un puerto, entonces primeo valida el puerto que recibe y si no tiene se le asigna el 300
app.set('views', path.join(__dirname, 'views'))    //__dirname : devuelve la carpeta src. luego de eso puedes concatenar con otra carpeta. Es decir l está indicando donde está la carpeta views ya que ahi iran los archivos html
app.engine('.hbs', exphbs({
    defaultLayout: 'main',  //aqui va plantilla principal de oda la aplicacion, cabecer, footer, etc
    layoutsDir: path.join(app.get('views'), 'layouts'),  //le esta concatenando pa saber onde est´ra ese archivo layouts
    partialsDir: path.join(app.get('views'), 'partials'),  //formulario para llamarlo en cada momento
    extname: '.hbs'  // sirve q extencion va tener los archivos
}));
app.set('view engine', '.hbs')   //para usar , moror de las vistas u platillas

//MIDDLEWARES   aqui van todo las funciones q seran ejecuadas antes que lleguen al servidor, pasrle a las rutas
app.use(express.urlencoded({extended: false}))   //cuando el cliente envie desde un formulario se pueda entender el user . false: para q no reciba fotos
app.use(methotOverride('_methot')); //para q lo formularios puedan enviar put o delete
app.use(session({   //para guadar los datos de los usuarios, van a permitir autenticar temporalmente
    secret: 'myscretapp',
    resave:true,
    saveUninitialized: true
}))
//GLOBAL ARIABLES   Apoder siertos datos q sean accesibles


//ROUTES
//urls q e usaran
app.use(require('./routes/index'));
app.use(require('./routes/notes'));
app.use(require('./routes/users'));

//STATIC FILES    archivos estaticos, dond estan
app.use(express.static(path.join(__dirname, 'public')))  //es para indicarle dode está la carpeta y decirle q esa carpeta es publica que puede usarse en cualquier otra parte


//SERVER IS LISTEN
app.listen(app.get('port'), () =>{
    console.log('Server on port', app.get('port'))   //va mostrar e console desde q puerto est+a escuchando
})