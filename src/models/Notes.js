//AQUIDEFINIMOS COMO LUCIR LOS DATOS Y DE AHI EMPEZAR A INSERTAR LOS DATOS

const mongoose = require('mongoose'); //llamar las librerias de MONGO/
const {Schema} = mongoose;   //CREAR ESQUEMa de datos

const NoteSchema = new Schema  ({   //definir como seran las notas, se vera las propiedads, es para decirle como lucira los datos, aun no sabe como crear el modelo
    title: {type: String, require:true},
    description: {type: String, require:true},
    date: {type: Date, default: Date.now}  //si no le pasa la fecha, este automaticamente le dara la fecha actual
})

module.exports = mongoose.model('Note', NoteSchema)  //aqui definimos el modelo. usar este modelo en otras partes de la aplicacion. NOTE :PUEDE SER USADO PARA ALMACENAR UNA NUEVA NOTA   SCHEMA